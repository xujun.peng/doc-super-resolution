This repo implements the approach described in "Building Super-Resolution Image Generator for OCR Accuracy Improvement"

To evaluate the pretrained model, please download it from:
https://pan.baidu.com/s/1GWIfu3bRsmwBP7Lu4VvgGA
