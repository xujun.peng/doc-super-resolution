import warnings

with warnings.catch_warnings():
    import cv2
    import csv
    import re
    import random
    import numpy as np
    from os.path import basename, splitext
    from wand.image import Image, Color
    from collections import Counter
    from sklearn.utils import shuffle
    from keras.utils import Sequence


class MiscDataLoader:
    def __init__(self, hr_dataset_path, lr_dataset_path, patch_size=(256, 256)):
        self.hr_db, self.lr_db = self._load_db(hr_dataset_path, lr_dataset_path)
        self.patch_size = patch_size

    def _load_db(self, hr_db, lr_db):
        hr_images, lr_images = [], []

        with open(hr_db, "r") as fh:
            reader = csv.reader(fh)
            for row in reader:
                hr_images.append(row[0])
        with open(lr_db, "r") as fh:
            reader = csv.reader(fh)
            for row in reader:
                lr_images.append(row[0])

        return hr_images, lr_images

    def load_data(self, batch_size):
        def select_patch(image_path, sample_num):
            image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
            mask = np.ones(image.shape, np.uint8) * 255
            mask[: -self.patch_size[0], : -self.patch_size[1]] = 0
            mask = mask | image
            coordinates = np.where(mask != [255])
            coordinates = list(zip(coordinates[0], coordinates[1]))
            indices = np.random.randint(0, len(coordinates), sample_num)

            X = []
            for i in indices:
                im = image[
                    coordinates[i][0] : coordinates[i][0] + self.patch_size[0],
                    coordinates[i][1] : coordinates[i][1] + self.patch_size[1],
                ]
                X.append(im)

            return X

        X, Y = [], []
        # samples from HR images
        samples = np.random.choice(random.sample(self.hr_db, 80), batch_size)
        counts = Counter(samples)
        for image_path in counts:
            x = select_patch(image_path, counts[image_path])
            X += x

        # samples from LR images
        samples = np.random.choice(random.sample(self.lr_db, 80), batch_size)
        counts = Counter(samples)
        for image_path in counts:
            y = select_patch(image_path, counts[image_path])
            Y += y

        X = np.reshape(X, (len(X), self.patch_size[1], self.patch_size[0], 1)).astype(
            "float"
        )
        Y = np.reshape(Y, (len(Y), self.patch_size[1], self.patch_size[0], 1)).astype(
            "float"
        )

        return shuffle(X, Y)
