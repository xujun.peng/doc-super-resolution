import os, sys, csv
import math
import cv2
import argparse
import random
import nets
import tensorflow as tf
import numpy as np
from keras import backend as K
from os.path import basename, splitext

patch_size = (256, 256)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = argparse.ArgumentParser(description="Document Super-resolution.")
    parser.add_argument("-i", "--input-csv", dest="input_db", help="The input dataset")
    parser.add_argument(
        "-m", "--model-file", dest="model", help="The pretrained CNN quality model"
    )
    parser.add_argument(
        "-o", "--output-dir", dest="out_dir", help="Output dir for restored image"
    )
    args = parser.parse_args()

    # using CPU
    config = tf.ConfigProto(device_count={"GPU": 0})
    sess = tf.Session(config=config)
    K.set_session(sess)

    # create network and copy weights from model
    if os.path.isfile(args.model):
        generator = nets.Generator(
            patch_size
        ).generator()  # build_generator(image_size)
        generator.load_weights(args.model)
    else:
        raise IOError(f"Load pre-trained model {args.model} failed.")

    with open(args.input_db, "r") as fh:
        # load input images
        reader = csv.reader(fh)
        for row in reader:
            file_name = splitext(basename(row[0]))[0]
            print(file_name)

            image = cv2.imread(row[0], cv2.IMREAD_GRAYSCALE)

            # divide images to overlapped blocks
            y_blocks = image.shape[0] // patch_size[0] + math.ceil(
                image.shape[0] % patch_size[0] / patch_size[0]
            )
            y_step = (image.shape[0] - patch_size[0]) // (y_blocks - 1)
            x_blocks = image.shape[1] // patch_size[1] + math.ceil(
                image.shape[1] % patch_size[1] / patch_size[1]
            )
            x_step = (image.shape[1] - patch_size[1]) // (x_blocks - 1)
            blocks = np.array(
                [
                    image[y : y + patch_size[0], x : x + patch_size[1]]
                    for y in range(0, image.shape[0] - patch_size[0], y_step)
                    for x in range(0, image.shape[1] - patch_size[1], x_step)
                ]
            )
            blocks = np.expand_dims(blocks, -1)
            generated = generator.predict(blocks)

            # merge blocks back to image
            for i, img in enumerate(generated):
                x, y = i % x_blocks, i // x_blocks
                image[
                    y * y_step : y * y_step + patch_size[0],
                    x * x_step : x * x_step + patch_size[1],
                ] = img[:, :, 0]
            cv2.imwrite(args.out_dir + "/" + file_name + ".hr.png", image)


if __name__ == "__main__":
    sys.exit(main())
